## intent:ask_me_question
- [ask-question]

## intent:ask_side_effect
- 有咩副作用
- 食完會有咩副用
- 其實系味會有副用作

## intent:want_to_die:
- 我想死

## intent:ask_futher_after_recovery
- 我有精神病後還有前途嗎？

## intent:ask_will_be_recovery
- 我會康復嗎？

## intent:ask_will_not_be_recovery
- 我係唔係會好唔番，要成世食藥？

## intent:concern_appearance+concern_health
- 啲藥係咪會令我變癡肥架？
- 食藥後肥左十幾kg，用咗好多方法都瘦唔到，點算好？
- 已經食少咗野，有去做運動同跑步，體重都仍然有增無減，到底係邊到出咗問題呢?

## intent:concern_health
- 啲藥會有咩副作用架？
- 睇醫生時間耐咗，係咪藥物份量會越食越多，副作用多啲？
- 咩係精神健康？

## intent:concern_safety
- 會唔會食食吓藥暴斃架？
- 食藥時間耐咗，係咪會死架？

## intent:deny
- 冇阿

## intent:feel_helpless_in_hospital
- 上次入院前，無人理我，屋企人又夾埋外人迫我，個時感到孤立無援

## intent:feel_no_futher_when_in_sick
- 有咗呢個病，我揾唔到好工，無曬前途囉，係咪啊？

## intent:goodbye
- bye
- goodbye
- see you around
- see you later
- byebye
- bye
- byebye
- 123
- 多謝晒你今日同我傾計, bye

## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there
- hi
- i am fine thank you and you?
- hi
- hi
- hi
- 早
- 早
- HI
- Hello
- 早晨
- 你好
- Hello
- 早晨
- 你好
- 你好
- 你好
- 你好
- 你好阿
- Hi
- test
- 你好

## intent:like_companion
- 多啲人一齊行好過自己一個捱
- 我可以同邊個傾訴？

## intent:like_to_share_to_stranger
- 我成日搵同路人傾計分享

## intent:mind_other_to_know_me_in_sick
- 我唔想話俾人知我有精神病 驚其他人用有色眼鏡睇我

## intent:no_one_understand_me
- 點解冇人明白我！個個都唔信我

## intent:not_want_get_scolded
- 成日都有人喺度鬧我！鬧到我喊！
- 我唔敢搵人傾，我驚會俾人鬧我癡線！

## intent:not_want_go_out
- 我根本冇動力出街！咩都唔想做！
- 我而家連睇醫生都好驚唔敢出門口，點算？

## intent:not_want_hurt_oneself
- 我好驚終有一日會整親自己！

## intent:worry_hurt_by_voice
- Ｄ聲音要我傷害自己，我可以點？

## intent:not_want_illness
- 我唔想咁樣病落去呀！

## intent:think_get_well_already
- 我最近都無乜問題啦
- 我有準時食藥

## intent:think_get_well_already+think_get_well_means_stop_medicine
- 我都冇再聽到啲怪聲啦，係咪可以停藥？
- 我覺得呢排心情好好，唔需要食藥啦！
- 我都好得七七八八啦，再食藥我好怕會依賴藥物！

## intent:unwell
- 我今日唔係好舒服

## intent:want_acceptance
- 佢哋仲會唔會當我係正常人咁看待？
- 我覺得自己一個係呢個世界係多餘嘅，點解仲要留喺度獻世？

## intent:want_get_healed+want_health+want_understand_medicine+want_no_medicine
- 啲藥點幫到我好番呀？幾時先可以唔食藥呀？
- 啲藥真係有效咩？我係咪成世都要食藥呀？
- 點為之穩定？

## intent:want_get_trust
- 我講真架！點解冇人信我？
- 呢度個個人都以為我精神病，冇人信我！

## intent:want_get_well+think_medicine_cannot_help
- 啲藥都幫唔到我嘅？我點樣先可以好番？

## intent:want_get_well+think_own_emotion_affecting_others
- 我啲情緒好飄忽，好似成日會影響到人，點先可以好番？

## intent:want_get_well+want_deal_with_voice+want_health+want_normal_life
- 啲聲好煩呀，我點先可以正常番呀？

## intent:want_get_well+want_happiness
- 點先可以好番開心番？

## intent:want_get_well+want_no_voice
- 周圍都嘈住晒，點先可以靜啲呀？
- 我成日覺得有人話緊我！點先可以整走佢哋呀？
- 成日有人同我講野

## intent:want_get_well+want_normal_life
- 我近排好好

## intent:want_get_well_on_ones_own
- 個病其實係咪自己會好番架？
- 我可唔可以靠做運動好番？
- 我可唔可以靠意志力好番？

## intent:want_get_well_soon
- 我幾時可以唔洗食藥好返曬？

## intent:want_get_well_soon+worry_cannot_get_well
- 我會唔會成世都係咁？幾時先好得番？
- 我今日好開心

## intent:want_give_birth
- 啲藥會唔會影響到我生BB？
- 啲藥會唔會令到我生畸形BB？
- 我個小朋友會唔會都有呢個病？

## intent:want_keep_good_relationship
- 我成日無心機出街，同屋企人嘅關係好似越嚟越遠
- 我覺得自己一個好慘好寂寞，就算死咗都冇人會傷心

## intent:want_maintain_good_interpersonal_relationship
- 我想正常咁同人行街、食飯、睇戲。

## intent:want_maintain_good_interpersonal_relationship+want_get_well
- 我Ｄ朋友宜家好少搵我，點算好？
- 點解我識親的好朋友最後都離我而去？我真係咁討厭嗎？
- 我想減肥

## intent:want_maintain_good_relationship_with_family
- 呢個病會唔會影響我同屋企人嘅關係？
- 我父母都唔支持我，我好唔開心。

## intent:want_maintain_normal_life
- 我會唔會搞到要住醫院架？
- 我近排好開心

## intent:want_no_illness_on_oneself
- 精神分裂會唔會突然發生喺自己身上？
- 精神分裂會唔會傳染？

## intent:want_normal+want_no_voice
- 我唔想再聽到啲聲喇！我點先可以正常番呀？
- 我去到邊，啲聲就跟到邊，點算好？

## intent:want_normal_life
- 我唔想食藥

## intent:want_normal_life+want_maintain_current_situation
- 我唔想入醫院，入醫院好似坐監咁

## intent:want_safety
- 職員嚟到會唔會出示委任證？

## intent:want_sleep
- 咁嘈我點瞓覺呀？

## intent:want_sleep+worry_addiction
- 我一唔食安眠藥就瞓得好差，咁係咪已經上咗癮喇？
- 呢幾日食左安眠藥都瞓唔到，好辛苦。

## intent:want_stop_medicine
- 我都已經無徵狀，幾時可以停藥呀？

## intent:want_trust
- 我真係聽到呢啲聲！我冇誇張！
- 點解無人肯信我講既嘢？
- 我今日唔舒服

## intent:want_without_pressure
- 點先可以釋放我而家嘅壓力？

## intent:want_without_pressure+want_get_well
- 點先可以好番冇咁大壓力？

## intent:want_without_problem+want_get_well
- 點先可以好番冇晒問題？

## intent:worry_having_illness+want_get_well
- 最近成日聽到啲唔知邊度嚟嘅聲，我係咪有病？我點先可以好番？

## intent:worry_having_illness+want_health
- 點解我會見到其他人見唔到嘅嘢？係唔係幻覺呀？
- 我係唔係癡咗線呀？
- 我會唔會死呀？

## intent:worry_safety
- 我好似俾人跟蹤緊，佢哋唔知想對我做啲乜嘢！
- 去餐廳食飯，有人偷聽我講嘢
- 好多人夾埋想害我，我好驚！

## intent:worry_hearing_voice
- 我成日聽到把聲同我傾計，係咪唔正常呀？

## intent:worry_hearing_voice_to_sick
- 點解我會聽到其他人聽唔到既聲音既？係唔係幻聽呀？

## intent:worry_hearing_voice_but_take_medicine
- 為何我食藥食左咁耐還有聲音？

## intent:worry_to_be_monitoring
- 我依排比樓上監視住，系屋企，佢地有蔽路電視，睇到我。D人等我唔系屋企時入嚟偷野。點算？ 

## intent:heard_too_much_noise
- 佢地經常發出D聲，有BB喊聲、揼嘢聲、鑽地聲、撞牆聲，等我瞓唔到覺。好煩啊﹗

## intent:how_to_handle_noise
- 我好頻煩聽到啲聲音，要點處理？

## intent:how_to_handle_noise2
- 聲音係咁騷擾我，我要點做？

## intent:ask_psycho_could_have_bady
- 精神疾病患者是否有資格生育？

## intent:think_did_not_get_psychosis
- 阿Sir，我不覺得自己有精神病，我真是自己聽到同事係我背後鬧我，就算我真是有精神病，咁又關你乜嘢事？我不想醫得唔得？

## intent:confuse_voice
- 我又再聽到神的聲音，究竟這是神的應召，還是幻覺？

## intent:wonder_why_still_hear_voice_in_treament
- 點解食咗藥咁耐都仲聽到聲音？

## intent:ask_could_not_take_medicine
- 食咗醫生D藥後非常眼訓，起唔到身返工，可唔可以唔再食藥？

## intent:think_did_not_get_psychosis2
- 我唔覺得我有病喎！係你地個個都唔信我...

## intent:annoyed_about_noise
- 啲聲音令覺得我好煩，阻住我工作。

## intent:want_stay_at_home
- 我唔想出街 淨係想留喺屋企瞓

## intent:ask_still_have_to_take_medicine
- 我係咪要食一世藥呀？

## intent:ask_to_stop_medicine
- 我宜家瞓得好好，係咪可以停一停藥呀？

## intent:worry_need_take_medicine_for_a_long_time
- 我瞓唔到覺，但又怕食醫生開既安眠藥食左會上癮。

## intent:ask_reduce_medicine
- 食左藥之後會覺得好攰同好眼瞓，影響到唔夠精神工作，我可唔可以食少啲份量藥或者唔食呀？

## intent:ask_why_medicine
- 食完藥有甚麼用呀？

## intent:ask_when_stop_medicine
- 我幾時可以唔洗食藥好返曬？

## intent:ask_when_to_be_care
- 我要睇幾耐醫生先會好返?

## intent:ask_stop_medicine
- 我可唔可以唔食藥?

## intent:ask_advice_when_hear_voice
- 我成日聽到有人同我講嘢，但又唔知係真定假，可以點做?

## intent:ask_take_chinese_medicine
- 我覺得西藥副作用好大，想轉食中藥，可以嗎？

## intent:ask_need_take_medicine_forever
- 食藥係咪要食一世？

## intent:stopped_medicine
- 啲藥食到我好累，我已經停咗啦

## intent:ask_why_need_to_take_medicine_if_stable
- 我的病情已經穩定，為何仍要服藥？

## intent:ask_stop_medicine_because_increased_weight
- 我食藥後體重增加了，要如何處理，可否停藥？

## intent:ask_medicine_side_effect
- 精神科藥物會對身體有害嗎?

## intent:ask_why_medicine_labelled_poison
- 為什麼藥盒上都貼上 “毒藥” 的字樣?

## intent:ask_really_take_medicine_forever
- 是否真係要食一生

## intent:ask_for_stop
- 醫生唔肯改,我想自己試停可以嗎？

## intent:ask_take_medicine_really_helpful
- 食足就真係一定好返？

## intent:ask_really_take_medicine_forever2
- 我係唔係成世都要食藥?

## intent:feel_bad_take_medicine
- 成世食藥都好慘姐?

## intent:ask_why_take_medicine_still_hear_voice
- 點解食左藥, 都仲聽到聲音(幻聽)?

## intent:ask_anyway_to_control_mind_except_medicine
- 除咗食藥仲有乜嘢方法可以管理好自己嘅情緒？

## intent:ask_still_mind_after_take_medicine
- 點解我食咗藥之後個情緒都係好波動?

## intent:ask_still_mind_after_take_medicine2
- 點解我即使食足晒藥，情緒都會突然變差？

## intent:ask_how_long_for_taking_medicine
- 我食藥要食幾耐？

## intent:ask_when_to_reduce_medicine
- 我幾時可以減藥？

## intent:ask_when_to_stop_medicine
- 我想問我仲要食藥食到幾時？

## intent:ask_why_sleepy_after_take_medicine
- 食咗藥之後我覺得好眼瞓點算？

## intent:ask_why_take_medicine_still_hear_voice2
- 點解食足藥都仲聽到聲？

## intent:ask_stop_medicine_because_not_hear_voice
- 而家我聽唔到聲，應該可以停藥？

## intent:ask_why_worse_after_medicine
- 點解食左藥個人好攰，轉數又慢，仲衰過唔食？

## intent:ask_if_heard_more_voice_need_to_take_more_medicine
- 我同你講最近聽多左聲，你會唔會即時話俾醫生聽，加我藥？

## intent:ask_why_heard_more_voice_after_chatting
- 點解有時同你傾完之後，會聽多左聲？

## intent:want_not_to_take_medicine
- 我唔想食藥呀！食咗藥有副作用，又攰又辛苦…

## intent:ask_when_stop_medicine2
- 究竟食藥要食到幾時?

## intent:ask_why_need_to_review_and_take_medicine
- 點解仲要覆診食藥! 好辛苦呀!

## intent:complain_taking_medicine
- 食藥好多副作用，但唔食又唔得

## intent:want_to_stop_medicine
- 我唔想食藥，食藥後好唔舒適

## intent:feedback_not_heard_voice_after_take_medicine
- 我食藥之後無聽到聲

## intent:ask_what_is_psychosis
- 精神分裂症是遺傳病嗎？

## intent:ask_how_serious
- 點解醫生叫你黎探我？因為我好嚴重？

## intent:ask_what_state_have_to_stay_in_hospital
- 在什麼情況下你們會送我入院？

## intent:ask_what_state_to_tell_my_family
- 你在甚麼情況下會將我們的對話內容告知我的家人？

## intent:ask_how_many_people_to_vist
- 每次會安排多少人來探訪？會否安排同性別的職員作家訪？

## intent:feel_too_pressure
- 我依家好大壓力， 唔想同人地講太多野。

## intent:feel_unhappy
- 我好唔開心，無人明白我，你可唔可以同我傾計？

## intent:ask_which_doctor_better
- 我想知道邊個精神科醫生好Ｄ?

## intent:worry_health 
- 我擔心自己有精神問題, 唔知點算好?

## intent:feel_too_pressure2
- 我依排工作壓力好大，晚晚失眠，好辛苦。 

## intent:feel_too_much_problem
- 我有好多問題，唔知同邊個講？

## intent:feel_no_one_understand
- 我想同人地講我Ｄ諗法，但佢地成日都唔明我請咩？ 

## intent:feel_be_hated
- Ｄ朋友成日話我發脾氣

## intent:want_to_sleep
- 我無心機出街，想瞓覺

## intent:want_being_healthy__5k2
- D人拎手機出黎就以為影你咁囉，對上頭一次發病，影你之後就驚，驚再之後就有E個病，所以我就驚，有時候望番後面，原來佢地唔係影你，睇緊上網D資料，有D人打機

## intent:want_being_healthy__6k2
- 見到D人拎住手機，初初就會覺得人地想影你，初初發病果時就唔想出街，想擋避，咁因為始終震返工，我返晚上至朝早收工，咁係第二次發病時候，就好少少，無咁驚

## intent:want_being_healthy__9k2
- 我宜家好肥！

## intent:want_being_healthy__10k2
- 我宜家好重！

## intent:ask_treatment_forever__13k2
- 係咪一定要準時食藥？

## intent:want_being_healthy__14k2
- 有嘔。差唔多每次食晏，成個午餐嘔番出黎

## intent:feel_healthy+feel_get_better__15k2
- 我無再食藥就嘔

## intent:want_being_healthy__22k2
- 唔想成日諗住果D野

## intent:want_being_healthy__25k2
- 我成日聽到把女聲

## intent:want_being_healthy__26k2
- 我聽到把男聲

## intent:want_being_healthy__27k2
- 聽到超級多把聲

## intent:want_being_respected__28k2
- 我放咗工無返屋企

## intent:want_get_well_with_no_side_effect__29k2
- 食藥有咩副作用？

## intent:want_being_healthy__33k2
- 經歷咗第一次復發啦，可以話第一次復發咗，第二次嘅時候，其實你會睇得到，你復發之後果種感受或者過程都不是好受

## intent:want_being_respected__34k2
- 我好無信心

## intent:want_being_respected__35k2
- 係無人讚過我

## intent:want_social_life__37k2
- 我想去呀，有無錢，有假無錢，好慘呀

## intent:feel_being_healthy__39k2
- 點解覺得我係一個病人?

## intent:want_being_healthy__40k2
- 我喺精神科中心,會點樣治療我?

## intent:want_being_healthy__41k2
- 我瞓覺瞓唔到、食野冇胃口係咪有精神病?

## intent:want_being_healthy__43k2
- 住精神科中心有冇其他福利攞架?

## intent:want_normal_social_life__45k2
- 我係精神病康復者,可以攞傷殘津貼定係綜援？

## intent:want_normal_social_life__46k2
- 如果我住精神科中心,可唔可以返去探屋企人？幾耐見一次

## intent:want_being_healthy__49k2
- 我可以返轉頭重新嚟過嗎?

## intent:want_being_respected__50k2
- 我點樣可以靠自己搵工?

## intent:want_being_healthy__51k2
- 要有咩信念先唔會再病發?

## intent:want_being_healthy+want_normal_social_life__53k2
- 我食左藥點解成日流口水? 好影響我日常生活

## intent:want_being_healthy__54k2
- 醫生有幫我打針, 係咩針嚟? 有咩用?

## intent:want_normal_social_life__56k2
- 護士幾耐見我一次？

## intent:want_being_healthy__58k2
- 我都想定期做運動,但食完藥好攰,做唔到,唔係我唔想做! 我可以點做?

## intent:want_being_healthy__59k2
- 我好累可以點樣舒緩自己？

## intent:want_being_healthy__60k2
- 我做完運動後心情上都冇改變, 只覺得好攰,出咗好多汗!!!!!

## intent:want_being_healthy__61k2
- 運動對病有咩好處?

## intent:want_normal_social_life__62k2
- 我住喺宿舍,可提出意見嗎?

## intent:want_normal_social_life__63k2
- 申請公屋要點樣申請？

## intent:want_being_respected__64k2
- 我都想自力更身,想靠自己，唔想攞綜援!!

## intent:want_normal_social_life__65k2
- 我得一個親人,唔係經常聯絡, 傾電話都少,有事時想傾下計, 可以搵邊個幫到我?

## intent:want_normal_social_life__66k2
- 可以長期住精神科中心或宿舍嗎?

## intent:want_normal_social_life__67k2
- 我已經好大年紀, 宿舍就快住完，我會住係邊? 

## intent:want_normal_social_life__68k2
- 點解我入住精神科宿舍後, 公屋被收返?

## intent:want_normal_social_life__69k2
- 住宿舍會係點? 職員好唔好架?

## intent:want_being_healthy__70k2
- 記性開始差差哋？

## intent:want_being_healthy__71k2
- 認唔到路,會迷路, 點算好?

## intent:want_normal_social_life+want_being_respected__72k2
- 揾唔到工，平時可以做啲乜？

## intent:want_normal_social_life__73k2
- 住宿舍係咪一定要幫手做社務?

## intent:want_normal_social_life__75k2
- 點解唔比我自己一個住,要我住宿舍?

## intent:want_being_healthy__76k2
- 點樣可以去睇精神科?

## intent:want_being_healthy__77k2
- 因為有長期病令我好痛,護士同醫生講話我有少少自殺傾向, 我都唔知點解會咁?

## intent:want_being_respected__78k2
- 過左退休年齡住完宿舍,又排唔到公屋, 要排護老院,但我唔想住護老院!

## intent:want_being_healthy__79k2
- 長期病令我想死!!

## intent:want_being_healthy__80k2
- 覺得自己冇得醫，冇乜希望!

## intent:want_being_healthy__81k2
- 社工擔心我做傻事!

## intent:want_being_respected+want_normal_social_life__82k2
- 咁住護老院有乜好處？

## intent:want_normal_social_life__83k2
- 咁住宿舍有乜好處？

## intent:want_being_healthy__84k2
- 我最擔心係會再病發!

## intent:want_being_respected__85k2
- 我都有可能到時去勞工處問下做保安得唔得，我就擔心XXX姑娘話過咗退休年齡

## intent:feel_being_healthy__91k2
- 我對我自己好鍾意啟發，好鍾意啟發自己

## intent:want_normal_social_life__93k2
- 我晚晚都玩電腦，上網

## intent:want_being_respected__94k2
- 我好唔鍾意D人話我食野慢

## intent:want_being_respected__95k2
- 我食野慢又關佢咩事？

## intent:want_being_healthy__96k2
- 星期六日我有去晨運

## intent:want_being_healthy+want_stop_treatment__97k2
- 我食藥好唔舒服好辛苦呀

## intent:want_being_respected__98k2
- 無人關心我呀

## intent:want_being_respected__99k2
- 我屋企人梗有人關心我啦

## intent:want_being_respected__101k2
- 我身邊D朋友糟質於我

## intent:want_being_respected__102k2
- 佢地話我唔靚

## intent:want_being_respected__103k2
- 佢地唔可以咁挑撥離間

## intent:want_being_respected__104k2
- 我想參加香港小姐比賽！

## intent:want_being_respected__105k2
- 我點知呀！我都無入過場

## intent:want_being_respected__106k2
- 有呀！好迷戀呢D! 

## intent:want_being_respected__107k2
- 我想個樣靚D 

## intent:want_being_respected__108k2
- 對於我黎講外表好重要

## intent:want_being_respected__109k2
- 自己過度自己嘅人生好重要

## intent:want_being_respected__112k2
- 我做過幾日麥當勞

## intent:want_being_respected__113k2
- 我唔識做呀嘛! 好麻煩 

## intent:want_being_respected__114k2
- 俾我諗幾秒

## intent:want_normal_social_life__115k2
- 我想搭飛機，去旅行

## intent:want_being_respected__116k2
- 我想做空中小姐，我覺得自己有資格

## intent:want_being_respected__117k2
- 熟能生巧等如有條有理

## intent:want_being_respected+want_hope+want_future_goal__118k2
- 你覺得自己是什麼的人？

## intent:want_being_respected__120k2
- 你覺得做香港小姐要點呀？

## intent:want_being_respected__121k2
- 你今年幾歲？

## intent:want_being_respected__122k2
- 你咁高但你咁後生？

## intent:want_being_respected__123k2
- 我覺得你講野好語無倫次

## intent:want_being_respected__124k2
- 可以改善下 

## intent:want_being_respected__127k2
- 讀緊大學

## intent:want_being_respected__129k2
- 好似讀極都讀咖到好辛苦咁樣，跟住果時拍拖，本身搬咗去男朋友果度住嘅，咁感情可能愈黎愈唔好啦，跟住搬番曬D野番屋企

## intent:want_being_respected__130k2
- 果時冇而家咁肥嘅，都係120Lb度啦，跟住自己係度諗係咪身型問題，跟住啪佐好多粒藥落去，變到好多幻覺呀

## intent:want_being_healthy+want_being_respected__131k2
- 減肥藥，上網買果D

## intent:want_being_healthy+want_being_respected__132k2
- 我食減肥藥

## intent:want_being_healthy+want_being_respected__133k2
- 想瘦 

## intent:want_being_healthy__134k2
- 有幻覺，將頭撞牆，又大叫，姑姐行過咪報警囉

## intent:want_being_healthy__135k2
- 唔係自殺

## intent:want_being_healthy__136k2
- 我有復診

## intent:want_being_healthy__137k2
- 係亞嫲上咗身講野，跟住去醫院，跟住唔肯返屋企，見到亞嫲張相呢又黎，咁安排咗我去宿舍住

## intent:want_being_healthy__138k2
- 好似上咗我身，用我口話唔俾我返黎，其實而家都仲有，不過食住藥，控制住

## intent:want_being_healthy__139k2
- 佢自己係咁講囉，好似做戲上身咁

## intent:want_being_healthy__140k2
- 知！係佢講唔係我講

## intent:want_being_healthy__141k2
- 會，變咗好恐怖，震下震下咁樣

## intent:want_being_healthy__142k2
- 食藥，我知道仲係度囉，但係就唔俾佢講囉

## intent:want_being_healthy_want_being_respected__143k2
- 屋企人好擔心，之後就覺得係我自己問題

## intent:want_being_healthy__144k2
- 唔係啊嘛，覺得係我病姐，之後話世界無鬼，邊有鬼，亞嫲死咁耐，點會而家上你身，果D說話

## intent:want_being_respected__145k2
- 我同佢一齊住，由細到大都係佢湊大我，但有時鬥氣，鬧下交咁樣囉

## intent:want_being_healthy__147k2
- 藥物根治唔到把聲，但起碼可控制佢唔俾佢講，因為未食藥之前，就係咁Up，Up，Up

## intent:want_being_healthy__148k2
- 有時都唔知佢Up咩，好惡咖，佢把聲，好震呀樣

## intent:want_being_healthy__149k2
- 嫲嫲會搖下搖下，咁樣搖

## intent:want_being_healthy__150k2
- 無咩特定時候，不過多數沖緊涼呀，吹緊頭果陣時

## intent:want_being_healthy__151k2
- 我會嘗試去控制佢

## intent:want_being_healthy__152k2
- 一想講果陣時，咪唔俾佢講囉，阻止佢囉

## intent:want_being_healthy__153k2
- 即刻走囉，做另一樣野，總之唔俾佢講

## intent:want_being_healthy__154k2
- 其實有第3者係度就可以止到

## intent:want_being_healthy__155k2
- 我唔俾佢講

## intent:want_being_healthy__156k2
- 會即刻唔講，驚俾佢地聽到

## intent:want_being_healthy__157k2
- 撞頭

## intent:want_being_healthy__158k2
- 驚嚇親D舍友，自己都驚啦

## intent:want_being_healthy__159k2
- 頭一次真係喺屋咗撞埋幅牆

## intent:want_being_respected__160k2
- 以家諗番都會有D後悔

## intent:want_being_respected__161k2
- 後悔點解當初咁樣囉

## intent:want_being_healthy__162k2
- 我覺得宜家咩都無

## intent:want_being_respected__163k2
- 最想做嘅係減肥囉

## intent:want_being_respected__164k2
- 因為宜家最困擾自己係果身形囉，成日唔敢出去搵野做呀，係囉

## intent:want_being_respected__165k2
- 太肥啦，驚俾人歧視

## intent:want_being_respected__166k2
- 我唔認同老闆D手法

## intent:want_being_respected__167k2
- 返功輔班果度呢D小朋友會叫我肥豬老師，跟住會問點解你咁肥，你係咪會食20碗飯每一日

## intent:want_being_respected__168k2
- 跟住開頭聽果陣會有D唔開心，但有時同屋企人食飯講呀，講完之後無事囉，都接受咗

## intent:want_being_respected__170k2
- 人地話我肥我會唔開心好耐囉

## intent:want_being_respected__172k2
- 以前唔係咁肥，姐係起碼肥過100磅，姐係自暴自棄咁樣囉，成日都想俾心機減嘅,成日3分鐘熱度囉，又好似無動力咁囉,自己有時照鏡都唔開心

## intent:want_being_respected__173k2
- 唔開心我會食更多野

## intent:want_being_healthy__175k2
- 有時真係好唔開心，好唔開心，喊唔出，頂多兩滴眼淚囉，你話嚎喊就未試過

## intent:want_being_healthy__176k2
- 我好在意自己好肥！

## intent:want_being_healthy__177k2
- 有咩方法減肥？

## intent:want_being_respected__178k2
- 我覺得自己好差 

## intent:want_being_respected__179k2
- 我對自己形象好在意

## intent:want_being_healthy__180k2
- 醫生話思覺失調，精神分裂

## intent:want_security__182k2
- 有人跟蹤我

## intent:want_security__183k2
- 周圍好似望住我

## intent:want_being_healthy__184k2
- 會不斷混亂我思緒，我想諗D正面D，佢就不斷混亂，叫我跟住佢做囉，好煩呀

## intent:want_being_respected+want_normal_social_life__186k2
- 果時返緊屋企，唔知咩美容，健身叫我入會，跟住申請信用卡，跟住申請之後無錢還啦，跟住自己返工果少少現金都無埋，跟住我就報警囉，咁住同佢地講埋唔知邊個偷佐我D錢咁樣講，然之後就話媽咪，然之後警察上黎就帶咗我去醫院咁樣囉

## intent:want_being_healthy__187k2
- 一直食藥都幫到我 

## intent:want_being_healthy__188k2
- 起碼食咗藥個人思緒正常D囉

## intent:want_being_healthy__189k2
- 個個人話叫我去死喎

## intent:want_being_healthy__190k2
- 我無理到佢囉

## intent:want_being_respected__193k2
- 我覺得我唔夠完美

## intent:want_being_respected__194k2
- 我放屁去廁所

## intent:want_being_respected__195k2
- 有時沖涼，係醫院，慣咗姐姐姑娘會問你果日洗唔洗頭咁樣，之後會叫你拎沖涼液，毛巾同埋衣服，咁出院之後，我每晚都會諗起姑娘講果D野，拎沖涼液，拎衣服，拎毛巾，同埋洗唔洗頭，果度呢成日都諗，因為紮辮，又要除辮，又帶錶，我而家無啦，所以成日諗E樣果樣，好心煩咁樣

## intent:want_normal_social_life__196k2
- 其實我想揀美容做嘅,但我妹妹話康復者難搵D

## intent:want_normal_social_life__197k2
- 我參加輔助就業

## intent:want_being_respected__198k2
- 精神病康復者係咪難搵工D？

## intent:want_normal_social_life__199k2
- 知唔知邊度請人？

## intent:want_normal_social_life__200k2
- 如果我搵唔到工，我去學其他野

## intent:want_being_respected+want_normal_social_life__201k2
- D朋友幫我啦，好多D朋友幫咗我，如果唔係好慘

## intent:want_normal_social_life__202k2
- 上樓過程都有困難

## intent:want_normal_social_life__206k2
- 唔好成日鬧交！脾氣發少

## intent:want_being_healthy+want_being_respected__207k2
- 我唔想成日發脾氣

## intent:want_being_healthy__209k2
- 坐立不安，做唔到野

## intent:want_being_healthy__210k2
- 睡眠方面要靠藥

## intent:want_being_healthy__211k2
- 如果唔食藥就訓唔著

## intent:want_being_healthy__212k2
- 出身汗沖熱水涼就舒服

## intent:want_being_healthy__213k2
- 做完運動幫到個人精神曬

## intent:want_being_healthy__214k2
- 畫禪繞畫

## intent:want_being_healthy__215k2
- 我有時畫下D公仔囉

## intent:want_being_healthy__216k2
- 畫花動物果D，花呀

## intent:want_being_healthy__217k2
- 自己畫自己上色

## intent:want_being_healthy+want_being_respected+want_normal_social_life__219k2
- 返咗中心就無咁忐忑，係屋企無咩做，成日坐係度

## intent:want_normal_social_life__220k2
- 上下網

## intent:want_being_healthy__221k2
- 試過抗拒食藥，之後病發，醫生都提一定要食藥

## intent:want_being_healthy__222k2
- 食野我好求其，食少少唔會食太多

## intent:want_being_healthy__223k2
- 最緊要有運動

## intent:want_being_healthy__224k2
- 我失眠，訓唔到覺

## intent:want_normal_social_life__225k2
- 畫畫

## intent:want_normal_social_life+want_being_respected__226k2
- 我要上課程

## intent:want_normal_social_life__227k2
- 果度係講好多資訊，等我知道而家果市場，唔同工作有關方面資訊，了解多D

## intent:want_normal_social_life__228k2
- 其實主要睇下上完D課程睇下有D咩工作

## intent:want_normal_social_life__229k2
- 我唔係好睇得到有咩變化

## intent:want_being_healthy__230k2
- 情緒成日都唔好，壓力大果時

## intent:want_being_healthy__231k2
- 情緒唔穩定

## intent:want_being_healthy__232k2
- 好大壓力呀

## intent:want_being_healthy__233k2
- 冇發出黎，情緒低落都會有

## intent:want_being_healthy+want_normal_social_life__234k2
- 變咗要花時間去睇醫生呀，跟住後尾要覆診，變咗都晒時間

## intent:want_being_healthy__235k2
- 工作壓力

## intent:want_being_respected__236k2
- 同同事唔夾

## intent:want_being_respected__246k2
- 僱主有個避忌歧視

## intent:want_being_respected__247k2
- 我唔適應我的工作 

## intent:want_being_respected__249k2
- 我好擔心我的工作 

## intent:want_being_healthy__253k2
- 近排成日聽到聲音呀

## intent:want_being_healthy__254k2
- 比之前多咗聲呀

## intent:want_being_healthy__255k2
- 既然係咁我係咪要加藥呢？

## intent:want_being_healthy__256k2
- 我要食幾耐藥？

## intent:feel_being_healthy__257k2
- 我其實想返工，正正常常，無人白眼我，又唔會俾老闆知E件事抄我，正常過生活，最好啦就唔使再食藥，同埋唔使再覆診，因為我睇私家都好貴

## intent:want_being_healthy+want_stop_treatment__258k2
- 如果食一世，因為我食藥D副作用都好犀利㗎，又手震啦，便秘，肥胖啦，好多好多副作用

## intent:want_being_healthy__259k2
- 我手震得好厲害

## intent:want_being_healthy+want_stop_treatment__260k2
- 食藥搞到我好肥呀

## intent:want_being_respected+want_being_healthy__261k2
- 其實我唔開心會同腦入面D朋友傾

## intent:want_being_respected__262k2
- 多謝腦裡D朋友，腦裡D朋友好真實，係我感覺上面

## intent:want_being_respected__263k2
- 屋企人都有嘅,屋企人有陣時亞爸亞媽都解決唔到，但我都有同佢地講，因為我都為勉佢地擔心

## intent:want_being_healthy__264k2
- 使唔做見營養師呀我？

## intent:want_being_respected__265k2
- 我有中途宿舍㗎嘛 ！

## intent:want_being_healthy__273k2
- 以前會 而家食 clozapine好有幫助好似好多病人咁啦 有啲年紀大啲 精神病人認識個病 控制個病容易多啲 我初時食clozapine都係清醒 但有時耐唔耐都會失眠 會瞓唔好 又會有緊張時間多啲始終對生活上嘅障礙, 都多但我呢十年嚟 轉食clozapine 500幾,呢十年晚晚都瞓到,呢十年都冇點緊張

## intent:want_being_healthy__274k2
- 有啲護士都講如果呢隻clozapine藥食都唔好,就冇乜藥食得好

## intent:want_being_healthy__275k2
- 淨係知個個月要抽血 !

## intent:want_being_healthy__276k2
- 流口水同便秘 

## intent:want_being_healthy__277k2
- 好似抽離驗白血球

## intent:want_being_healthy__278k2
- 便秘可以點處理?

## intent:want_being_healthy__279k2
- 越緊張會越便秘

## intent:want_being_respected__280k2
- 住宿舍冇工作能力啦搵唔到工,會安排我去邊工作?

## intent:want_normal_social_life__281k2
- 庇護工場做D咩?

## intent:want_being_respected__282k2
- 有咩人會一齊做?

## intent:want_being_respected__283k2
- 庇護工場做嘢要不停咁做? 食咗藥攰點算?

## intent:want_security__284k2
- 社會咁亂, 睇到新聞都擔心,瞓唔著

## intent:want_security+want_being_respected__285k2
- 我好驚得返自己一個

## intent:want_being_respected+want_normal_social_life__286k2
- 年紀大中途宿舍會趕我走嗎?

## intent:want_being_respected__287k2
- 住宿舍係咪冇自由?

## intent:want_being_respected__288k2
- 長期護老院難排嗎?

## intent:want_being_respected__289k2
- 住宿舍要做入面舍嘅清潔嗎?

## intent:want_being_respected__290k2
- 庇護工場可以做到幾多歲

## intent:want_being_respected__291k2
- 喺庇護工場做嘢有幾多人工?

## intent:want_being_respected__292k2
- 有呢個病都3,4十年,想退休,可以做得D咩?

## intent:want_being_healthy__293k2
- 想問點解咁耐唔好? 食藥食到幾時?

## intent:want_being_healthy__294k2
- 長時期食藥有咩影響?

## intent:want_being_healthy__295k2
- 精神病有咩特徵?

## synonym:係 唔 係
- 係唔係

## synonym:唔 想 食藥
- 唔想食藥

## synonym:唔 食藥
- 唔食藥
