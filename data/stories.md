## interactive_story_1
* greet
    - utter_greet
    - action_restart

## ask_me_question
* ask_me_question
    - utter_greet
    - action_restart

## interactive_story_1
* goodbye
    - utter_goodbye
    - action_restart

## want_stop_medicine
* want_stop_medicine
    - utter_want_stop_medicine
    - action_restart

## ask_futher_after_recovery
* ask_futher_after_recovery
    - utter_ask_futher_after_recovery
    - action_restart

## feel_helpless_in_hospital
* feel_helpless_in_hospital
    - utter_feel_helpless_in_hospital
    - action_restart

## feel_no_futher_when_in_sick
* feel_no_futher_when_in_sick
    - utter_feel_no_futher_when_in_sick
    - action_restart

## ask_will_be_recovery
* ask_will_be_recovery
    - utter_ask_will_be_recovery
    - action_restart

## ask_will_not_be_recovery 
* ask_will_not_be_recovery
    - utter_ask_will_not_be_recovery
    - action_restart

## mind_other_to_know_me_in_sick
* mind_other_to_know_me_in_sick
    - utter_mind_other_to_know_me_in_sick
    - action_restart

## no_one_understand_me
* no_one_understand_me
    - utter_no_one_understand_me
    - action_restart

## like_to_share_to_stranger
* like_to_share_to_stranger
    - utter_like_to_share_to_stranger
    - action_restart

## worry_hearing_voice
* worry_hearing_voice
    - utter_worry_hearing_voice
    - action_restart

## worry_hearing_voice_to_sick
* worry_hearing_voice_to_sick
    - utter_worry_hearing_voice_to_sick
    - action_restart

## worry_hearing_voice_but_take_medicine
* worry_hearing_voice_but_take_medicine
    - utter_worry_hearing_voice_but_take_medicine
    - action_restart

## worry_to_be_monitoring 
* worry_to_be_monitoring
    - utter_worry_to_be_monitoring
    - action_restart

## heard_too_much_noise 
* heard_too_much_noise
    - utter_heard_too_much_noise
    - action_restart

## how_to_handle_noise
* how_to_handle_noise
    - utter_how_to_handle_noise
    - action_restart

## how_to_handle_noise2
* how_to_handle_noise2
    - utter_how_to_handle_noise2
    - action_restart

## ask_psycho_could_have_bady 
* ask_psycho_could_have_bady
    - utter_ask_psycho_could_have_bady
    - action_restart

## think_did_not_get_psychosis
* think_did_not_get_psychosis
    - utter_think_did_not_get_psychosis
    - action_restart

## confuse_voice
* confuse_voice
    - utter_confuse_voice
    - action_restart

## wonder_why_still_hear_voice_in_treament
* wonder_why_still_hear_voice_in_treament
    - utter_wonder_why_still_hear_voice_in_treament
    - action_restart

## ask_could_not_take_medicine
* ask_could_not_take_medicine
    - utter_ask_could_not_take_medicine
    - action_restart

## think_did_not_get_psychosis2
* think_did_not_get_psychosis2
    - utter_think_did_not_get_psychosis2
    - action_restart

## annoyed_about_noise
* annoyed_about_noise
    - utter_annoyed_about_noise
    - action_restart

## want_stay_at_home
* want_stay_at_home
    - utter_want_stay_at_home
    - action_restart

## ask_still_have_to_take_medicine
* ask_still_have_to_take_medicine
    - utter_ask_still_have_to_take_medicine
    - action_restart

## ask_to_stop_medicine
* ask_to_stop_medicine
    - utter_ask_to_stop_medicine
    - action_restart

## worry_need_take_medicine_for_a_long_time
* worry_need_take_medicine_for_a_long_time
    - utter_worry_need_take_medicine_for_a_long_time
    - action_restart

## ask_reduce_medicine
* ask_reduce_medicine
    - utter_ask_reduce_medicine
    - action_restart

## ask_why_medicine
* ask_why_medicine
    - utter_ask_why_medicine
    - action_restart

## ask_when_stop_medicine
* ask_when_stop_medicine
    - utter_ask_when_stop_medicine
    - action_restart

## ask_when_to_be_care
* ask_when_to_be_care
    - utter_ask_when_to_be_care
    - action_restart

## ask_stop_medicine
* ask_stop_medicine
    - utter_ask_stop_medicine
    - action_restart

## ask_advice_when_hear_voice
* ask_advice_when_hear_voice
    - utter_ask_advice_when_hear_voice
    - action_restart

## ask_take_chinese_medicine
* ask_take_chinese_medicine
    - utter_ask_take_chinese_medicine
    - action_restart

## ask_need_take_medicine_forever
* ask_need_take_medicine_forever
    - utter_ask_need_take_medicine_forever
    - action_restart

## stopped_medicine
* stopped_medicine
    - utter_stopped_medicine
    - action_restart

## ask_why_need_to_take_medicine_if_stable
* ask_why_need_to_take_medicine_if_stable
    - utter_ask_why_need_to_take_medicine_if_stable
    - action_restart

## ask_stop_medicine_because_increased_weight
* ask_stop_medicine_because_increased_weight
    - utter_ask_stop_medicine_because_increased_weight
    - action_restart

## ask_medicine_side_effect
* ask_medicine_side_effect
    - utter_ask_medicine_side_effect
    - action_restart

## ask_why_medicine_labelled_poison
* ask_why_medicine_labelled_poison
    - utter_ask_why_medicine_labelled_poison
    - action_restart

## ask_really_take_medicine_forever
* ask_really_take_medicine_forever
    - utter_ask_really_take_medicine_forever
    - action_restart

## ask_for_stop
* ask_for_stop
    - utter_ask_for_stop
    - action_restart

## ask_take_medicine_really_helpful
* ask_take_medicine_really_helpful
    - utter_ask_take_medicine_really_helpful
    - action_restart

## ask_really_take_medicine_forever2
* ask_really_take_medicine_forever2
    - utter_ask_really_take_medicine_forever2
    - action_restart

## feel_bad_take_medicine
* feel_bad_take_medicine
    - utter_feel_bad_take_medicine
    - action_restart

## ask_why_take_medicine_still_hear_voice
* ask_why_take_medicine_still_hear_voice
    - utter_ask_why_take_medicine_still_hear_voice
    - action_restart

## ask_anyway_to_control_mind_except_medicine
* ask_anyway_to_control_mind_except_medicine
    - utter_ask_anyway_to_control_mind_except_medicine
    - action_restart

## ask_still_mind_after_take_medicine
* ask_still_mind_after_take_medicine
    - utter_ask_still_mind_after_take_medicine
    - action_restart

## ask_still_mind_after_take_medicine2
* ask_still_mind_after_take_medicine2
    - utter_ask_still_mind_after_take_medicine2
    - action_restart

## ask_how_long_for_taking_medicine
* ask_how_long_for_taking_medicine
    - utter_ask_how_long_for_taking_medicine
    - action_restart

## ask_when_to_reduce_medicine
* ask_when_to_reduce_medicine
    - utter_ask_when_to_reduce_medicine
    - action_restart

## ask_when_to_stop_medicine
* ask_when_to_stop_medicine
    - utter_ask_when_to_stop_medicine
    - action_restart

## ask_why_sleepy_after_take_medicine
* ask_why_sleepy_after_take_medicine
    - utter_ask_why_sleepy_after_take_medicine
    - action_restart

## ask_why_take_medicine_still_hear_voice2
* ask_why_take_medicine_still_hear_voice2
    - utter_ask_why_take_medicine_still_hear_voice2
    - action_restart

## ask_stop_medicine_because_not_hear_voice
* ask_stop_medicine_because_not_hear_voice
    - utter_ask_stop_medicine_because_not_hear_voice
    - action_restart

## ask_why_worse_after_medicine
* ask_why_worse_after_medicine
    - utter_ask_why_worse_after_medicine
    - action_restart

## ask_if_heard_more_voice_need_to_take_more_medicine
* ask_if_heard_more_voice_need_to_take_more_medicine
    - utter_ask_if_heard_more_voice_need_to_take_more_medicine
    - action_restart

## ask_why_heard_more_voice_after_chatting
* ask_why_heard_more_voice_after_chatting
    - utter_ask_why_heard_more_voice_after_chatting
    - action_restart

## want_not_to_take_medicine
* want_not_to_take_medicine
    - utter_want_not_to_take_medicine
    - action_restart

## ask_when_stop_medicine2
* ask_when_stop_medicine2
    - utter_ask_when_stop_medicine2
    - action_restart

## ask_why_need_to_review_and_take_medicine
* ask_why_need_to_review_and_take_medicine
    - utter_ask_why_need_to_review_and_take_medicine
    - action_restart

## complain_taking_medicine
* complain_taking_medicine
    - utter_complain_taking_medicine
    - action_restart

## want_to_stop_medicine
* want_to_stop_medicine
    - utter_want_to_stop_medicine
    - action_restart

## feedback_not_heard_voice_after_take_medicine
* feedback_not_heard_voice_after_take_medicine
    - utter_feedback_not_heard_voice_after_take_medicine
    - action_restart

## ask_what_is_psychosis
* ask_what_is_psychosis
    - utter_ask_what_is_psychosis
    - action_restart

## ask_how_serious
* ask_how_serious
    - utter_ask_how_serious
    - action_restart

## ask_what_state_have_to_stay_in_hospital
* ask_what_state_have_to_stay_in_hospital
    - utter_ask_what_state_have_to_stay_in_hospital
    - action_restart

## ask_what_state_to_tell_my_family
* ask_what_state_to_tell_my_family
    - utter_ask_what_state_to_tell_my_family
    - action_restart

## ask_how_many_people_to_vist
* ask_how_many_people_to_vist
    - utter_ask_how_many_people_to_vist
    - action_restart

## feel_too_pressure
* feel_too_pressure
    - utter_feel_too_pressure
    - action_restart

## feel_unhappy
* feel_unhappy
    - utter_feel_unhappy
    - action_restart

## ask_which_doctor_better
* ask_which_doctor_better
    - utter_ask_which_doctor_better
    - action_restart

## worry_health
* worry_health 
    - utter_worry_health
    - action_restart

## feel_too_pressure2
* feel_too_pressure2
    - utter_feel_too_pressure2
    - action_restart

## feel_too_much_problem
* feel_too_much_problem
    - utter_feel_too_much_problem
    - action_restart

## feel_no_one_understand
* feel_no_one_understand
    - utter_feel_no_one_understand
    - action_restart

## feel_be_hated
* feel_be_hated
    - utter_feel_be_hated
    - action_restart

## want_to_sleep
* want_to_sleep
    - utter_want_to_sleep
    - action_restart

## worry_hurt_by_voice
* worry_hurt_by_voice
    - utter_worry_hurt_by_voice
    - action_restart

## ask_side_effect
* ask_side_effect
    - utter_ask_side_effect
    - action_restart

## want_being_healthy__5k2
* want_being_healthy__5k2
    - utter_want_being_healthy__5k2
    - action_restart

## want_being_healthy__6k2
* want_being_healthy__6k2
    - utter_want_being_healthy__6k2
    - action_restart

## want_being_healthy__9k2
* want_being_healthy__9k2
    - utter_want_being_healthy__9k2
    - action_restart

## want_being_healthy__10k2
* want_being_healthy__10k2
    - utter_want_being_healthy__10k2
    - action_restart

## ask_treatment_forever__13k2
* ask_treatment_forever__13k2
    - utter_ask_treatment_forever__13k2
    - action_restart

## want_being_healthy__14k2
* want_being_healthy__14k2
    - utter_want_being_healthy__14k2
    - action_restart

## feel_healthy+feel_get_better__15k2
* feel_healthy+feel_get_better__15k2
    - utter_feel_healthy+feel_get_better__15k2
    - action_restart

## want_being_healthy__22k2
* want_being_healthy__22k2
    - utter_want_being_healthy__22k2
    - action_restart

## want_being_healthy__25k2
* want_being_healthy__25k2
    - utter_want_being_healthy__25k2
    - action_restart

## want_being_healthy__26k2
* want_being_healthy__26k2
    - utter_want_being_healthy__26k2
    - action_restart

## want_being_healthy__27k2
* want_being_healthy__27k2
    - utter_want_being_healthy__27k2
    - action_restart

## want_being_respected__28k2
* want_being_respected__28k2
    - utter_want_being_respected__28k2
    - action_restart

## want_get_well_with_no_side_effect__29k2
* want_get_well_with_no_side_effect__29k2
    - utter_want_get_well_with_no_side_effect__29k2
    - action_restart

## want_being_healthy__33k2
* want_being_healthy__33k2
    - utter_want_being_healthy__33k2
    - action_restart

## want_being_respected__34k2
* want_being_respected__34k2
    - utter_want_being_respected__34k2
    - action_restart

## want_being_respected__35k2
* want_being_respected__35k2
    - utter_want_being_respected__35k2
    - action_restart

## want_social_life__37k2
* want_social_life__37k2
    - utter_want_social_life__37k2
    - action_restart

## feel_being_healthy__39k2
* feel_being_healthy__39k2
    - utter_feel_being_healthy__39k2
    - action_restart

## want_being_healthy__40k2
* want_being_healthy__40k2
    - utter_want_being_healthy__40k2
    - action_restart

## want_being_healthy__41k2
* want_being_healthy__41k2
    - utter_want_being_healthy__41k2
    - action_restart

## want_being_healthy__43k2
* want_being_healthy__43k2
    - utter_want_being_healthy__43k2
    - action_restart

## want_normal_social_life__45k2
* want_normal_social_life__45k2
    - utter_want_normal_social_life__45k2
    - action_restart

## want_normal_social_life__46k2
* want_normal_social_life__46k2
    - utter_want_normal_social_life__46k2
    - action_restart

## want_being_healthy__49k2
* want_being_healthy__49k2
    - utter_want_being_healthy__49k2
    - action_restart

## want_being_respected__50k2
* want_being_respected__50k2
    - utter_want_being_respected__50k2
    - action_restart

## want_being_healthy__51k2
* want_being_healthy__51k2
    - utter_want_being_healthy__51k2
    - action_restart

## want_being_healthy+want_normal_social_life__53k2
* want_being_healthy+want_normal_social_life__53k2
    - utter_want_being_healthy+want_normal_social_life__53k2
    - action_restart

## want_being_healthy__54k2
* want_being_healthy__54k2
    - utter_want_being_healthy__54k2
    - action_restart

## want_normal_social_life__56k2
* want_normal_social_life__56k2
    - utter_want_normal_social_life__56k2
    - action_restart

## want_being_healthy__58k2
* want_being_healthy__58k2
    - utter_want_being_healthy__58k2
    - action_restart

## want_being_healthy__59k2
* want_being_healthy__59k2
    - utter_want_being_healthy__59k2
    - action_restart

## want_being_healthy__60k2
* want_being_healthy__60k2
    - utter_want_being_healthy__60k2
    - action_restart

## want_being_healthy__61k2
* want_being_healthy__61k2
    - utter_want_being_healthy__61k2
    - action_restart

## want_normal_social_life__62k2
* want_normal_social_life__62k2
    - utter_want_normal_social_life__62k2
    - action_restart

## want_normal_social_life__63k2
* want_normal_social_life__63k2
    - utter_want_normal_social_life__63k2
    - action_restart

## want_being_respected__64k2
* want_being_respected__64k2
    - utter_want_being_respected__64k2
    - action_restart

## want_normal_social_life__65k2
* want_normal_social_life__65k2
    - utter_want_normal_social_life__65k2
    - action_restart

## want_normal_social_life__66k2
* want_normal_social_life__66k2
    - utter_want_normal_social_life__66k2
    - action_restart

## want_normal_social_life__67k2
* want_normal_social_life__67k2
    - utter_want_normal_social_life__67k2
    - action_restart

## want_normal_social_life__68k2
* want_normal_social_life__68k2
    - utter_want_normal_social_life__68k2
    - action_restart

## want_normal_social_life__69k2
* want_normal_social_life__69k2
    - utter_want_normal_social_life__69k2
    - action_restart

## want_being_healthy__70k2
* want_being_healthy__70k2
    - utter_want_being_healthy__70k2
    - action_restart

## want_being_healthy__71k2
* want_being_healthy__71k2
    - utter_want_being_healthy__71k2
    - action_restart

## want_normal_social_life+want_being_respected__72k2
* want_normal_social_life+want_being_respected__72k2
    - utter_want_normal_social_life+want_being_respected__72k2
    - action_restart

## want_normal_social_life__73k2
* want_normal_social_life__73k2
    - utter_want_normal_social_life__73k2
    - action_restart

## want_normal_social_life__75k2
* want_normal_social_life__75k2
    - utter_want_normal_social_life__75k2
    - action_restart

## want_being_healthy__76k2
* want_being_healthy__76k2
    - utter_want_being_healthy__76k2
    - action_restart

## want_being_healthy__77k2
* want_being_healthy__77k2
    - utter_want_being_healthy__77k2
    - action_restart

## want_being_respected__78k2
* want_being_respected__78k2
    - utter_want_being_respected__78k2
    - action_restart

## want_being_healthy__79k2
* want_being_healthy__79k2
    - utter_want_being_healthy__79k2
    - action_restart

## want_being_healthy__80k2
* want_being_healthy__80k2
    - utter_want_being_healthy__80k2
    - action_restart

## want_being_healthy__81k2
* want_being_healthy__81k2
    - utter_want_being_healthy__81k2
    - action_restart

## want_being_respected+want_normal_social_life__82k2
* want_being_respected+want_normal_social_life__82k2
    - utter_want_being_respected+want_normal_social_life__82k2
    - action_restart

## want_normal_social_life__83k2
* want_normal_social_life__83k2
    - utter_want_normal_social_life__83k2
    - action_restart

## want_being_healthy__84k2
* want_being_healthy__84k2
    - utter_want_being_healthy__84k2
    - action_restart

## want_being_respected__85k2
* want_being_respected__85k2
    - utter_want_being_respected__85k2
    - action_restart

## feel_being_healthy__91k2
* feel_being_healthy__91k2
    - utter_feel_being_healthy__91k2
    - action_restart

## want_normal_social_life__93k2
* want_normal_social_life__93k2
    - utter_want_normal_social_life__93k2
    - action_restart

## want_being_respected__94k2
* want_being_respected__94k2
    - utter_want_being_respected__94k2
    - action_restart

## want_being_respected__95k2
* want_being_respected__95k2
    - utter_want_being_respected__95k2
    - action_restart

## want_being_healthy__96k2
* want_being_healthy__96k2
    - utter_want_being_healthy__96k2
    - action_restart

## want_being_healthy+want_stop_treatment__97k2
* want_being_healthy+want_stop_treatment__97k2
    - utter_want_being_healthy+want_stop_treatment__97k2
    - action_restart

## want_being_respected__98k2
* want_being_respected__98k2
    - utter_want_being_respected__98k2
    - action_restart

## want_being_respected__99k2
* want_being_respected__99k2
    - utter_want_being_respected__99k2
    - action_restart

## want_being_respected__101k2
* want_being_respected__101k2
    - utter_want_being_respected__101k2
    - action_restart

## want_being_respected__102k2
* want_being_respected__102k2
    - utter_want_being_respected__102k2
    - action_restart

## want_being_respected__103k2
* want_being_respected__103k2
    - utter_want_being_respected__103k2
    - action_restart

## want_being_respected__104k2
* want_being_respected__104k2
    - utter_want_being_respected__104k2
    - action_restart

## want_being_respected__105k2
* want_being_respected__105k2
    - utter_want_being_respected__105k2
    - action_restart

## want_being_respected__106k2
* want_being_respected__106k2
    - utter_want_being_respected__106k2
    - action_restart

## want_being_respected__107k2
* want_being_respected__107k2
    - utter_want_being_respected__107k2
    - action_restart

## want_being_respected__108k2
* want_being_respected__108k2
    - utter_want_being_respected__108k2
    - action_restart

## want_being_respected__109k2
* want_being_respected__109k2
    - utter_want_being_respected__109k2
    - action_restart

## want_being_respected__112k2
* want_being_respected__112k2
    - utter_want_being_respected__112k2
    - action_restart

## want_being_respected__113k2
* want_being_respected__113k2
    - utter_want_being_respected__113k2
    - action_restart

## want_being_respected__114k2
* want_being_respected__114k2
    - utter_want_being_respected__114k2
    - action_restart

## want_normal_social_life__115k2
* want_normal_social_life__115k2
    - utter_want_normal_social_life__115k2
    - action_restart

## want_being_respected__116k2
* want_being_respected__116k2
    - utter_want_being_respected__116k2
    - action_restart

## want_being_respected__117k2
* want_being_respected__117k2
    - utter_want_being_respected__117k2
    - action_restart

## want_being_respected+want_hope+want_future_goal__118k2
* want_being_respected+want_hope+want_future_goal__118k2
    - utter_want_being_respected+want_hope+want_future_goal__118k2
    - action_restart

## want_being_respected__120k2
* want_being_respected__120k2
    - utter_want_being_respected__120k2
    - action_restart

## want_being_respected__121k2
* want_being_respected__121k2
    - utter_want_being_respected__121k2
    - action_restart

## want_being_respected__122k2
* want_being_respected__122k2
    - utter_want_being_respected__122k2
    - action_restart

## want_being_respected__123k2
* want_being_respected__123k2
    - utter_want_being_respected__123k2
    - action_restart

## want_being_respected__124k2
* want_being_respected__124k2
    - utter_want_being_respected__124k2
    - action_restart

## want_being_respected__127k2
* want_being_respected__127k2
    - utter_want_being_respected__127k2
    - action_restart

## want_being_respected__129k2
* want_being_respected__129k2
    - utter_want_being_respected__129k2
    - action_restart

## want_being_respected__130k2
* want_being_respected__130k2
    - utter_want_being_respected__130k2
    - action_restart

## want_being_healthy+want_being_respected__131k2
* want_being_healthy+want_being_respected__131k2
    - utter_want_being_healthy+want_being_respected__131k2
    - action_restart

## want_being_healthy+want_being_respected__132k2
* want_being_healthy+want_being_respected__132k2
    - utter_want_being_healthy+want_being_respected__132k2
    - action_restart

## want_being_healthy+want_being_respected__133k2
* want_being_healthy+want_being_respected__133k2
    - utter_want_being_healthy+want_being_respected__133k2
    - action_restart

## want_being_healthy__134k2
* want_being_healthy__134k2
    - utter_want_being_healthy__134k2
    - action_restart

## want_being_healthy__135k2
* want_being_healthy__135k2
    - utter_want_being_healthy__135k2
    - action_restart

## want_being_healthy__136k2
* want_being_healthy__136k2
    - utter_want_being_healthy__136k2
    - action_restart

## want_being_healthy__137k2
* want_being_healthy__137k2
    - utter_want_being_healthy__137k2
    - action_restart

## want_being_healthy__138k2
* want_being_healthy__138k2
    - utter_want_being_healthy__138k2
    - action_restart

## want_being_healthy__139k2
* want_being_healthy__139k2
    - utter_want_being_healthy__139k2
    - action_restart

## want_being_healthy__140k2
* want_being_healthy__140k2
    - utter_want_being_healthy__140k2
    - action_restart

## want_being_healthy__141k2
* want_being_healthy__141k2
    - utter_want_being_healthy__141k2
    - action_restart

## want_being_healthy__142k2
* want_being_healthy__142k2
    - utter_want_being_healthy__142k2
    - action_restart

## want_being_healthy_want_being_respected__143k2
* want_being_healthy_want_being_respected__143k2
    - utter_want_being_healthy_want_being_respected__143k2
    - action_restart

## want_being_healthy__144k2
* want_being_healthy__144k2
    - utter_want_being_healthy__144k2
    - action_restart

## want_being_respected__145k2
* want_being_respected__145k2
    - utter_want_being_respected__145k2
    - action_restart

## want_being_healthy__147k2
* want_being_healthy__147k2
    - utter_want_being_healthy__147k2
    - action_restart

## want_being_healthy__148k2
* want_being_healthy__148k2
    - utter_want_being_healthy__148k2
    - action_restart

## want_being_healthy__149k2
* want_being_healthy__149k2
    - utter_want_being_healthy__149k2
    - action_restart

## want_being_healthy__150k2
* want_being_healthy__150k2
    - utter_want_being_healthy__150k2
    - action_restart

## want_being_healthy__151k2
* want_being_healthy__151k2
    - utter_want_being_healthy__151k2
    - action_restart

## want_being_healthy__152k2
* want_being_healthy__152k2
    - utter_want_being_healthy__152k2
    - action_restart

## want_being_healthy__153k2
* want_being_healthy__153k2
    - utter_want_being_healthy__153k2
    - action_restart

## want_being_healthy__154k2
* want_being_healthy__154k2
    - utter_want_being_healthy__154k2
    - action_restart

## want_being_healthy__155k2
* want_being_healthy__155k2
    - utter_want_being_healthy__155k2
    - action_restart

## want_being_healthy__156k2
* want_being_healthy__156k2
    - utter_want_being_healthy__156k2
    - action_restart

## want_being_healthy__157k2
* want_being_healthy__157k2
    - utter_want_being_healthy__157k2
    - action_restart

## want_being_healthy__158k2
* want_being_healthy__158k2
    - utter_want_being_healthy__158k2
    - action_restart

## want_being_healthy__159k2
* want_being_healthy__159k2
    - utter_want_being_healthy__159k2
    - action_restart

## want_being_respected__160k2
* want_being_respected__160k2
    - utter_want_being_respected__160k2
    - action_restart

## want_being_respected__161k2
* want_being_respected__161k2
    - utter_want_being_respected__161k2
    - action_restart

## want_being_healthy__162k2
* want_being_healthy__162k2
    - utter_want_being_healthy__162k2
    - action_restart

## want_being_respected__163k2
* want_being_respected__163k2
    - utter_want_being_respected__163k2
    - action_restart

## want_being_respected__164k2
* want_being_respected__164k2
    - utter_want_being_respected__164k2
    - action_restart

## want_being_respected__165k2
* want_being_respected__165k2
    - utter_want_being_respected__165k2
    - action_restart

## want_being_respected__166k2
* want_being_respected__166k2
    - utter_want_being_respected__166k2
    - action_restart

## want_being_respected__167k2
* want_being_respected__167k2
    - utter_want_being_respected__167k2
    - action_restart

## want_being_respected__168k2
* want_being_respected__168k2
    - utter_want_being_respected__168k2
    - action_restart

## want_being_respected__170k2
* want_being_respected__170k2
    - utter_want_being_respected__170k2
    - action_restart

## want_being_respected__172k2
* want_being_respected__172k2
    - utter_want_being_respected__172k2
    - action_restart

## want_being_respected__173k2
* want_being_respected__173k2
    - utter_want_being_respected__173k2
    - action_restart

## want_being_healthy__175k2
* want_being_healthy__175k2
    - utter_want_being_healthy__175k2
    - action_restart

## want_being_healthy__176k2
* want_being_healthy__176k2
    - utter_want_being_healthy__176k2
    - action_restart

## want_being_healthy__177k2
* want_being_healthy__177k2
    - utter_want_being_healthy__177k2
    - action_restart

## want_being_respected__178k2
* want_being_respected__178k2
    - utter_want_being_respected__178k2
    - action_restart

## want_being_respected__179k2
* want_being_respected__179k2
    - utter_want_being_respected__179k2
    - action_restart

## want_being_healthy__180k2
* want_being_healthy__180k2
    - utter_want_being_healthy__180k2
    - action_restart

## want_security__182k2
* want_security__182k2
    - utter_want_security__182k2
    - action_restart

## want_security__183k2
* want_security__183k2
    - utter_want_security__183k2
    - action_restart

## want_being_healthy__184k2
* want_being_healthy__184k2
    - utter_want_being_healthy__184k2
    - action_restart

## want_being_respected+want_normal_social_life__186k2
* want_being_respected+want_normal_social_life__186k2
    - utter_want_being_respected+want_normal_social_life__186k2
    - action_restart

## want_being_healthy__187k2
* want_being_healthy__187k2
    - utter_want_being_healthy__187k2
    - action_restart

## want_being_healthy__188k2
* want_being_healthy__188k2
    - utter_want_being_healthy__188k2
    - action_restart

## want_being_healthy__189k2
* want_being_healthy__189k2
    - utter_want_being_healthy__189k2
    - action_restart

## want_being_healthy__190k2
* want_being_healthy__190k2
    - utter_want_being_healthy__190k2
    - action_restart

## want_being_respected__193k2
* want_being_respected__193k2
    - utter_want_being_respected__193k2
    - action_restart

## want_being_respected__194k2
* want_being_respected__194k2
    - utter_want_being_respected__194k2
    - action_restart

## want_being_respected__195k2
* want_being_respected__195k2
    - utter_want_being_respected__195k2
    - action_restart

## want_normal_social_life__196k2
* want_normal_social_life__196k2
    - utter_want_normal_social_life__196k2
    - action_restart

## want_normal_social_life__197k2
* want_normal_social_life__197k2
    - utter_want_normal_social_life__197k2
    - action_restart

## want_being_respected__198k2
* want_being_respected__198k2
    - utter_want_being_respected__198k2
    - action_restart

## want_normal_social_life__199k2
* want_normal_social_life__199k2
    - utter_want_normal_social_life__199k2
    - action_restart

## want_normal_social_life__200k2
* want_normal_social_life__200k2
    - utter_want_normal_social_life__200k2
    - action_restart

## want_being_respected+want_normal_social_life__201k2
* want_being_respected+want_normal_social_life__201k2
    - utter_want_being_respected+want_normal_social_life__201k2
    - action_restart

## want_normal_social_life__202k2
* want_normal_social_life__202k2
    - utter_want_normal_social_life__202k2
    - action_restart

## want_normal_social_life__206k2
* want_normal_social_life__206k2
    - utter_want_normal_social_life__206k2
    - action_restart

## want_being_healthy+want_being_respected__207k2
* want_being_healthy+want_being_respected__207k2
    - utter_want_being_healthy+want_being_respected__207k2
    - action_restart

## want_being_healthy__209k2
* want_being_healthy__209k2
    - utter_want_being_healthy__209k2
    - action_restart

## want_being_healthy__210k2
* want_being_healthy__210k2
    - utter_want_being_healthy__210k2
    - action_restart

## want_being_healthy__211k2
* want_being_healthy__211k2
    - utter_want_being_healthy__211k2
    - action_restart

## want_being_healthy__212k2
* want_being_healthy__212k2
    - utter_want_being_healthy__212k2
    - action_restart

## want_being_healthy__213k2
* want_being_healthy__213k2
    - utter_want_being_healthy__213k2
    - action_restart

## want_being_healthy__214k2
* want_being_healthy__214k2
    - utter_want_being_healthy__214k2
    - action_restart

## want_being_healthy__215k2
* want_being_healthy__215k2
    - utter_want_being_healthy__215k2
    - action_restart

## want_being_healthy__216k2
* want_being_healthy__216k2
    - utter_want_being_healthy__216k2
    - action_restart

## want_being_healthy__217k2
* want_being_healthy__217k2
    - utter_want_being_healthy__217k2
    - action_restart

## want_being_healthy+want_being_respected+want_normal_social_life__219k2
* want_being_healthy+want_being_respected+want_normal_social_life__219k2
    - utter_want_being_healthy+want_being_respected+want_normal_social_life__219k2
    - action_restart

## want_normal_social_life__220k2
* want_normal_social_life__220k2
    - utter_want_normal_social_life__220k2
    - action_restart

## want_being_healthy__221k2
* want_being_healthy__221k2
    - utter_want_being_healthy__221k2
    - action_restart

## want_being_healthy__222k2
* want_being_healthy__222k2
    - utter_want_being_healthy__222k2
    - action_restart

## want_being_healthy__223k2
* want_being_healthy__223k2
    - utter_want_being_healthy__223k2
    - action_restart

## want_being_healthy__224k2
* want_being_healthy__224k2
    - utter_want_being_healthy__224k2
    - action_restart

## want_normal_social_life__225k2
* want_normal_social_life__225k2
    - utter_want_normal_social_life__225k2
    - action_restart

## want_normal_social_life+want_being_respected__226k2
* want_normal_social_life+want_being_respected__226k2
    - utter_want_normal_social_life+want_being_respected__226k2
    - action_restart

## want_normal_social_life__227k2
* want_normal_social_life__227k2
    - utter_want_normal_social_life__227k2
    - action_restart

## want_normal_social_life__228k2
* want_normal_social_life__228k2
    - utter_want_normal_social_life__228k2
    - action_restart

## want_normal_social_life__229k2
* want_normal_social_life__229k2
    - utter_want_normal_social_life__229k2
    - action_restart

## want_being_healthy__230k2
* want_being_healthy__230k2
    - utter_want_being_healthy__230k2
    - action_restart

## want_being_healthy__231k2
* want_being_healthy__231k2
    - utter_want_being_healthy__231k2
    - action_restart

## want_being_healthy__232k2
* want_being_healthy__232k2
    - utter_want_being_healthy__232k2
    - action_restart

## want_being_healthy__233k2
* want_being_healthy__233k2
    - utter_want_being_healthy__233k2
    - action_restart

## want_being_healthy+want_normal_social_life__234k2
* want_being_healthy+want_normal_social_life__234k2
    - utter_want_being_healthy+want_normal_social_life__234k2
    - action_restart

## want_being_healthy__235k2
* want_being_healthy__235k2
    - utter_want_being_healthy__235k2
    - action_restart

## want_being_respected__236k2
* want_being_respected__236k2
    - utter_want_being_respected__236k2
    - action_restart

## want_being_respected__246k2
* want_being_respected__246k2
    - utter_want_being_respected__246k2
    - action_restart

## want_being_respected__247k2
* want_being_respected__247k2
    - utter_want_being_respected__247k2
    - action_restart

## want_being_respected__249k2
* want_being_respected__249k2
    - utter_want_being_respected__249k2
    - action_restart

## want_being_healthy__253k2
* want_being_healthy__253k2
    - utter_want_being_healthy__253k2
    - action_restart

## want_being_healthy__254k2
* want_being_healthy__254k2
    - utter_want_being_healthy__254k2
    - action_restart

## want_being_healthy__255k2
* want_being_healthy__255k2
    - utter_want_being_healthy__255k2
    - action_restart

## want_being_healthy__256k2
* want_being_healthy__256k2
    - utter_want_being_healthy__256k2
    - action_restart

## feel_being_healthy__257k2
* feel_being_healthy__257k2
    - utter_feel_being_healthy__257k2
    - action_restart

## want_being_healthy+want_stop_treatment__258k2
* want_being_healthy+want_stop_treatment__258k2
    - utter_want_being_healthy+want_stop_treatment__258k2
    - action_restart

## want_being_healthy__259k2
* want_being_healthy__259k2
    - utter_want_being_healthy__259k2
    - action_restart

## want_being_healthy+want_stop_treatment__260k2
* want_being_healthy+want_stop_treatment__260k2
    - utter_want_being_healthy+want_stop_treatment__260k2
    - action_restart

## want_being_respected+want_being_healthy__261k2
* want_being_respected+want_being_healthy__261k2
    - utter_want_being_respected+want_being_healthy__261k2
    - action_restart

## want_being_respected__262k2
* want_being_respected__262k2
    - utter_want_being_respected__262k2
    - action_restart

## want_being_respected__263k2
* want_being_respected__263k2
    - utter_want_being_respected__263k2
    - action_restart

## want_being_healthy__264k2
* want_being_healthy__264k2
    - utter_want_being_healthy__264k2
    - action_restart

## want_being_respected__265k2
* want_being_respected__265k2
    - utter_want_being_respected__265k2
    - action_restart

## want_being_healthy__273k2
* want_being_healthy__273k2
    - utter_want_being_healthy__273k2
    - action_restart

## want_being_healthy__274k2
* want_being_healthy__274k2
    - utter_want_being_healthy__274k2
    - action_restart

## want_being_healthy__275k2
* want_being_healthy__275k2
    - utter_want_being_healthy__275k2
    - action_restart

## want_being_healthy__276k2
* want_being_healthy__276k2
    - utter_want_being_healthy__276k2
    - action_restart

## want_being_healthy__277k2
* want_being_healthy__277k2
    - utter_want_being_healthy__277k2
    - action_restart

## want_being_healthy__278k2
* want_being_healthy__278k2
    - utter_want_being_healthy__278k2
    - action_restart

## want_being_healthy__279k2
* want_being_healthy__279k2
    - utter_want_being_healthy__279k2
    - action_restart

## want_being_respected__280k2
* want_being_respected__280k2
    - utter_want_being_respected__280k2
    - action_restart

## want_normal_social_life__281k2
* want_normal_social_life__281k2
    - utter_want_normal_social_life__281k2
    - action_restart

## want_being_respected__282k2
* want_being_respected__282k2
    - utter_want_being_respected__282k2
    - action_restart

## want_being_respected__283k2
* want_being_respected__283k2
    - utter_want_being_respected__283k2
    - action_restart

## want_security__284k2
* want_security__284k2
    - utter_want_security__284k2
    - action_restart

## want_security+want_being_respected__285k2
* want_security+want_being_respected__285k2
    - utter_want_security+want_being_respected__285k2
    - action_restart

## want_being_respected+want_normal_social_life__286k2
* want_being_respected+want_normal_social_life__286k2
    - utter_want_being_respected+want_normal_social_life__286k2
    - action_restart

## want_being_respected__287k2
* want_being_respected__287k2
    - utter_want_being_respected__287k2
    - action_restart

## want_being_respected__288k2
* want_being_respected__288k2
    - utter_want_being_respected__288k2
    - action_restart

## want_being_respected__289k2
* want_being_respected__289k2
    - utter_want_being_respected__289k2
    - action_restart

## want_being_respected__290k2
* want_being_respected__290k2
    - utter_want_being_respected__290k2
    - action_restart

## want_being_respected__291k2
* want_being_respected__291k2
    - utter_want_being_respected__291k2
    - action_restart

## want_being_respected__292k2
* want_being_respected__292k2
    - utter_want_being_respected__292k2
    - action_restart

## want_being_healthy__293k2
* want_being_healthy__293k2
    - utter_want_being_healthy__293k2
    - action_restart

## want_being_healthy__294k2
* want_being_healthy__294k2
    - utter_want_being_healthy__294k2
    - action_restart

## want_being_healthy__295k2
* want_being_healthy__295k2
    - utter_want_being_healthy__295k2
    - action_restart

## want_want_to_die
* want_to_die
    - utter_want_to_die
    - action_restart
