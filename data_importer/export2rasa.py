# -*- coding: utf-8 -*-
import csv

def export2rasa():
    intent_lines = []

    domain_intent_lines = []
    domain_lines = []
    domain_action_lines = []

    story_lines = []

    with open('k2.csv') as csvfile:
        spamreader = csv.reader(csvfile)
        # skip header
        next(spamreader)
        for i, row in enumerate(spamreader):
            intent, intent_text, action, action_text, _ = row
            if intent_text and action_text:
                _intent = '%s__%dk2' % (intent.replace(' ', ''), i)
                intent_lines.append('## intent:%s\n' % _intent)
                intent_lines.append('- %s\n' % intent_text.replace('\n', ''))
                intent_lines.append('\n')

                _utter = 'utter_%s' % _intent
                domain_intent_lines.append('- %s\n' % _intent)
                domain_lines.append('  %s:\n' % _utter)
                domain_lines.append('  - text: %s\n' % action_text.replace('\n', '').replace(' ', ''))
                domain_action_lines.append('- %s\n' % _utter)

                story_lines.append('## %s\n' % _intent)
                story_lines.append('* %s\n' % _intent)
                story_lines.append('    - %s\n' % _utter)
                story_lines.append('\n')

    with open('nlu.md', 'w') as f:
        f.writelines(intent_lines)

    with open('domain.yml', 'w') as f:
        f.write('intents:\n')
        f.writelines(domain_intent_lines)
        f.write('templates:\n')
        f.writelines(domain_lines)
        f.write('actions:\n')
        f.writelines(domain_action_lines)

    with open('stories.md', 'w') as f:
        f.writelines(story_lines)


if __name__ == "__main__":
    export2rasa()